<?php

/**
 * HumHub
 * Copyright © 2014 The HumHub Project
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */

/**
 * Description of JsPanelsEvents
 *
 * @author luke
 */
class JsPanelsEvents
{
    public static function onAdminMenuInit($event)
    {
		$event->sender->addItem(array(
            'label' => Yii::t('JsPanelsModule.base', 'JS Panels'),
            'url' => Yii::app()->createUrl('//js_panels/admin'),
            'group' => 'manage',
            'icon' => '<i class="fa fa-file-o"></i>',
            'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'js_panels' && Yii::app()->controller->id == 'admin'),
            'sortOrder' => 300,
        ));

        // Check for Admin Menu Pages to insert
    }

    public static function onTopMenuInit($event)
    {
		foreach (JsPanel::model()->findAll() as $page) {

            // Admin only
            if ($page->admin_only == 1 && !Yii::app()->user->isAdmin()) {
                continue;
            }
			//print_r($event->sender);
            $event->sender->addItem(array(
                'label' => $page->title,
				'id' => 'slideJsPanels',
                'url' => Yii::app()->createUrl('//js_panels/view', array('id' => $page->id)),
				'target' => ($page->type == JsPanel::TYPE_LINK) ? '_blank' : '',
                'icon' => '<i class="fa ' . $page->icon . '"></i>',
                'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'js_panels' && Yii::app()->controller->id == 'view' && Yii::app()->request->getParam('id') == $page->id),
                'sortOrder' => ($page->sort_order != '') ? $page->sort_order : 1000,
            ));
        }
		
   
    }
}
