<?php
return array (
  'Create new Page' => 'Nieuwe pagina',
  'Slide Pages' => 'Aangepaste pagina\'s',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Link',
  'MarkDown' => 'Markdown',
  'Navigation' => 'Navigatie',
  'No Slide Pages created yet!' => 'Geen aangepaste pagina\'s tot nu toe!',
  'Sort Order' => '',
  'Title' => 'Titel',
  'Top Navigation' => 'Hoofd navigatie',
  'Type' => 'Type',
  'User Account Menu (Settings)' => 'Gebruikers account menu (Opties) ',
);
