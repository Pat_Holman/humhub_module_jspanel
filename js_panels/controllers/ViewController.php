<?php
/**
 * HumHub
 * Copyright © 2014 The HumHub Project
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */

/**
 * Description of ViewController
 *
 * @author luke
 */
class ViewController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {

		$page = JsPanel::model()->findByPk(Yii::app()->request->getParam('id'));
		
        if ($page === null) {
            throw new CHttpException('404', 'Could not find requested page');
        }

        if  ($page->admin_only == 1 && !Yii::app()->user->isAdmin()) {
            throw new CHttpException(403, 'Access denied!');
        }
		
		$returnArray = array(
			'navigationClass' => $page->navigation_class,
			'position'        => $page->position,
			'theme'        	  => $page->theme,
			'width'           => $page->width,
			'height'          => $page->height,
			'title'           => $page->title
		);
		$returnType = '';
       
        if( $page->type == JsPanel::TYPE_HTML || $page->type == JsPanel::TYPE_EMBED ) {		
			$returnType = 'html';
			$returnArray['html'] = $page->content; 
        } elseif ($page->type == JsPanel::TYPE_IFRAME) {			
			$returnType = 'iframe';
			$returnArray['url'] = $page->content;             
		} elseif ($page->type == JsPanel::TYPE_VIDEO) {			
			$returnType = 'video';
			$returnArray['url'] = $page->content;             
        } elseif ($page->type == JsPanel::TYPE_MARKDOWN) {
			$returnType = 'markdown';
			$returnArray['md'] = $page->content;             
        }
		$this->render($returnType,$returnArray);
		
    }




}
