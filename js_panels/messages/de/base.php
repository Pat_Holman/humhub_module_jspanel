<?php
return array (
  'Create new Page' => 'Neue Seite erstellen',
  'Slide Pages' => 'Eigene Seite',
  'HTML' => 'HTML',
  'IFrame' => 'iFrame',
  'Link' => 'Link',
  'MarkDown' => 'MarkDown',
  'Navigation' => 'Navigation',
  'No Slide Pages created yet!' => 'Bisher wurde noch keine eigene Seite erstellt!',
  'Sort Order' => 'Sortierung',
  'Title' => 'Titel',
  'Top Navigation' => 'Obere Navigationsleiste',
  'Type' => 'Typ',
  'User Account Menu (Settings)' => 'Benutzerprofilmenü (Einstellungen)',
);
