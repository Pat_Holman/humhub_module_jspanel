$(document).ready(function()
{
	$('.slideJsPanels').click(function(e)
	{
		e.preventDefault();
		var href_link	 = $(this).find('a').attr('href');		
		$('.visible-md').removeClass('active');
		$(this).addClass('active');		
		
		$.getJSON( href_link, function(content ) 
		{			
			$.jsPanel({
				//selector: '.container',
				size : { width: content.width, height: content.height },
				title: content.title,
				position: content.position,
				content: '<div style="padding:8px; height:100%;">'+content.content+'</div>',
				theme: content.theme
			});
		});		
	});
});