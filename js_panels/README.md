JS PANELS
============

Allows admins to add JS PANEL Pages (html, markdown, iframe, video) to top navigation.

__Status:__ alpha
__Module website:__ <https://github.com/humhub/humhub-modules-custom-pages>
__Author:__ Dinesh Laller
__Author website:__ [contriverz.com](http://contriverz.com)


For more  informations visit:
<https://github.com/humhub/humhub-modules-custom-pages>
