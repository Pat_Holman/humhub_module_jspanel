<?php

class m141053_235537_adminOnly extends EDbMigration
{

    public function up()
    {
        $this->addColumn('js_panels_page', 'admin_only', 'boolean DEFAULT 0');
    }

    public function down()
    {
        echo "m141026_235537_adminOnly does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
