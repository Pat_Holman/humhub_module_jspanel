<?php
return array (
  'Create new Page' => 'Создать новую страницу',
  'Slide Pages' => 'Персональные страницы',
  'HTML' => 'HTML',
  'IFrame' => 'Фрейм',
  'Link' => 'Ссылка',
  'MarkDown' => 'Форматирование',
  'Navigation' => 'Навигация',
  'No Slide Pages created yet!' => 'Персональные страницы пока не созданы!',
  'Sort Order' => 'Порядок сортировки',
  'Title' => 'Название',
  'Top Navigation' => 'Лучшие',
  'Type' => 'Тип',
  'User Account Menu (Settings)' => 'Аккаунт пользователя (настройки)',
);
