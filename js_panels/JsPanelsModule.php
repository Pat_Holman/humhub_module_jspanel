<?php
class JsPanelsModule extends HWebModule
{
	public $subLayout = "application.modules_core.user.views.account._layout";
	
	private $_assetsUrl;

	public function getConfigUrl()
    {
        return Yii::app()->createUrl('//js_panels/admin');
    }

    public function disable()
    {
        if (parent::disable()) {

            foreach (JsPanel::model()->findAll() as $entry) {
                $entry->delete();
            }
            return true;
        }
        return false;
    }

	/**
	* @register a js and css file.
	*/

	public function init()
	{
		$this->getAssetsUrl();
		Yii::app()->clientScript->registerScriptFile($this->_assetsUrl . '/js/custom_sliders.js');
		Yii::app()->clientScript->registerCssFile($this->_assetsUrl . '/css/custome_pages.css');		
		Yii::app()->clientScript->registerScriptFile($this->_assetsUrl . '/js/jquery-ui.js');
		Yii::app()->clientScript->registerCssFile($this->_assetsUrl . '/css/jquery-ui.css');
		Yii::app()->clientScript->registerScriptFile($this->_assetsUrl . '/js/jquery.jspanel.js');
		Yii::app()->clientScript->registerCssFile($this->_assetsUrl . '/css/jquery.jspanel.css');
	}

	/**
	* @return string the base URL that contains all published asset files of this module.
	*/
	public function getAssetsUrl()
	{
		if($this->_assetsUrl===null)
		   $this->_assetsUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('js_panels.assets'));
		   return $this->_assetsUrl;
	}

	/**
	 * @param string the base URL that contains all published asset files of this module.
	 */
	public function setAssetsUrl($value)
	{
		$this->_assetsUrl=$value;
	}

	public function registerImage($file)
	{
		return $this->getAssetsUrl().'/img/'.$file;
	}
	public function registerImg($file)
	{
		return $this->getAssetsUrl().'/images/'.$file;
	}
 
}
