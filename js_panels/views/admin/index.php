<div class="panel panel-default">
    <div class="panel-heading"><?php echo Yii::t('JsPanelsModule.base', 'Slide Pages'); ?></div>
    <div class="panel-body">
        <?php echo HHtml::link(Yii::t('JsPanelsModule.base', 'Create new Page'), $this->createUrl('edit'), array('class' => 'btn btn-primary')); ?>
        <p/>
        <p/>
        <?php if (count($pages) != 0): ?>
            <?php
            $positions = JsPanel::getPositionTypes();
            $types = JsPanel::getPageTypes();
            ?>
            <table class="table">
                <tr>
                    <th><?php echo Yii::t('JsPanelsModule.base', 'Title'); ?></th>
                    <th><?php echo Yii::t('JsPanelsModule.base', 'Slide Position'); ?></th>
                    <th><?php echo Yii::t('JsPanelsModule.base', 'Type'); ?></th>
					<th><?php echo Yii::t('JsPanelsModule.base', 'Theme'); ?></th>
					<th><?php echo Yii::t('JsPanelsModule.base', 'Width'); ?></th>
					<th><?php echo Yii::t('JsPanelsModule.base', 'Height'); ?></th>
                    <th><?php echo Yii::t('JsPanelsModule.base', 'Sort Order'); ?></th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($pages as $page): ?>
                    <tr>
                        <td><i class="fa <?php echo $page->icon; ?>"></i> <?php echo HHtml::link($page->title, $this->createUrl('edit', array('id' => $page->id))); ?></td>
                        <td><?php echo $positions[$page->position]; ?></td>
                        <td><?php echo $types[$page->type]; ?></td>
						<td><?php echo $page->theme; ?></td>
						<td><?php echo $page->width; ?></td>
						<td><?php echo $page->height; ?></td>
                        <td><?php echo $page->sort_order; ?></td>
                        <td><?php echo HHtml::link('Edit', $this->createUrl('edit', array('id' => $page->id)), array('class' => 'btn btn-primary btn-xs pull-right')); ?></td>
                    </tr>
					<tr>
                        <td colspan="5" class="slideJsPanels"><a href="<?php echo Yii::app()->createUrl('//js_panels/view', array('id' => $page->id));?>"><?php echo Yii::app()->createUrl('//js_panels/view', array('id' => $page->id));?></a></td>
                        </tr>

                <?php endforeach; ?>
            </table>

        <?php else: ?>

            <p><?php echo Yii::t('JsPanelsModule.base', 'No Slide Pages created yet!'); ?></p>


        <?php endif; ?>

    </div>
</div>


