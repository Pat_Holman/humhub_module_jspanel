<?php
	$returnArray = array(
		'navigationClass' => $navigationClass,
		'position'        => $position,
		'theme'        	  => $theme,
		'width'           => $width,
		'height'          => $height,
		'title'           => $title,
		'content'		  => $html
	);
	echo json_encode($returnArray);
	die;
?>