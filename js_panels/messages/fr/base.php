<?php
return array (
  'Create new Page' => 'Créer une nouvelle page',
  'Slide Pages' => 'Pages personnalisées',
  'HTML' => '',
  'IFrame' => '',
  'Link' => 'Lien',
  'MarkDown' => '',
  'Navigation' => '',
  'No Slide Pages created yet!' => 'Aucune page personnalisée n\'a été créée !',
  'Sort Order' => 'Ordre de tri',
  'Title' => 'Titre',
  'Top Navigation' => '',
  'Type' => '',
  'User Account Menu (Settings)' => '',
);
