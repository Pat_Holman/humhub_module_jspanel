<?php
Yii::app()->moduleManager->register(array(
    'id' => 'js_panels',
    'class' => 'application.modules.js_panels.JsPanelsModule',
    'import' => array(
        'application.modules.js_panels.*',
        'application.modules.js_panels.models.*',
		'application.modules.js_panels.assets.*',
		
    ),
    // Events to Catch 
    'events' => array(
        array('class' => 'AdminMenuWidget', 'event' => 'onInit', 'callback' => array('JsPanelsEvents', 'onAdminMenuInit')),
        array('class' => 'TopMenuWidget', 'event' => 'onInit', 'callback' => array('JsPanelsEvents', 'onTopMenuInit'))       
    ),
));
?>