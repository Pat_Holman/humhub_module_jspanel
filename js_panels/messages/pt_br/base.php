<?php
return array (
  'Create new Page' => 'Criar nova página',
  'Slide Pages' => 'Personalizar Páginas',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Link',
  'MarkDown' => 'MarkDown',
  'Navigation' => 'Navegação',
  'No Slide Pages created yet!' => 'Não há páginas personalizadas criadas ainda!',
  'Sort Order' => 'Ordem de Classificação',
  'Title' => 'Título',
  'Top Navigation' => 'Menu Superior',
  'Type' => 'Tipo',
  'User Account Menu (Settings)' => 'Menu da Conta de Usuário (Configurações)',
);
