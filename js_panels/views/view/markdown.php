<?php
	$returnArray = array(
		'navigationClass' => $navigationClass,
		'position'        => $position,
		'theme'        	  => $theme,
		'width'           => $width,
		'title'           => $title,
		'height'          => $height			
	);	
	
	$parser = new CMarkdownParser;
	$returnArray['content'] =  $parser->safeTransform($md);
	
	echo json_encode($returnArray);
	die;
?>