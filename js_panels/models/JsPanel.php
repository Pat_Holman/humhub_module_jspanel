<?php

/**
 * This is the model class for table "js_panels_page".
 *
 * The followings are the available columns in table 'js_panels_page':
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $icon
 * @property string $content
 * @property integer $sort_order
 * @property integer $admin_only
 * @property string $navigation_class
 * @position integer $position
 */
class JsPanel extends HActiveRecord
{

    public $url;

    const TYPE_LINK     = '1';
    const TYPE_HTML 	= '2';
    const TYPE_IFRAME 	= '3';
    const TYPE_MARKDOWN = '4';
	const TYPE_VIDEO	= '5';
	const TYPE_EMBED 	= '6';

	const POSITION_AUTO 		= 'auto';
    const POSITION_CENTER 		= 'center';
    const POSITION_TOPLEFT 		= 'top left';
    const POSITION_TOPCENTER 	= 'top center';
	const POSITION_TOPRIGHT 	= 'top right';
	const POSITION_CENTERRIGHT 	= 'center right';
	const POSITION_BOTTOMRIGHT 	= 'bottom right';
	const POSITION_BOTTOMCENTER = 'bottom center';
	const POSITION_BOTTOMLEFT 	= 'bottom left';
	const POSITION_CENTERLEFT 	= 'center left';
	
	const THEMES_LIGHT 			= 'light';
	const THEMES_MEDIUM 		= 'medium';
	const THEMES_DARK 			= 'dark';
	const THEMES_AUTUMNGREEN 	= 'autumngreen';
	const THEMES_AUTUMNBROWN 	= 'autumnbrown';
	const THEMES_AUTUMNRED 		= 'autumnred';
	const THEMES_PRIMARY 		= 'primary';
	const THEMES_SUCCESS 		= 'success';
	const THEMES_INFO 			= 'info';
	const THEMES_WARNING 		= 'warning';
	const THEMES_DANGER 		= 'danger';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return JsPanel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'js_panels_page';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, position, title', 'required'),
            array('type, sort_order, admin_only, width, height', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('icon', 'length', 'max' => 100),
            array('content, url, theme', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' 				=> 'ID',
            'type' 				=> 'Type',
			'position' 			=> 'Slide Positions',
            'title' 			=> 'Title',
			'theme' 			=> 'Theme',
			'Width' 			=> 'Width',
			'Height' 			=> 'Height',
            'icon' 				=> 'Icon',
            'content'			=> 'Content',
            'url' 				=> 'URL',
            'sort_order'		=> 'Sort Order',
            'admin_only' 		=> 'Only visible for admins',
            'navigation_class'  => 'Navigation',
        );
    }

    public function beforeSave()
    {
        if( $this->type == self::TYPE_IFRAME 
			|| $this->type == self::TYPE_VIDEO 
			|| $this->type == self::TYPE_LINK ) {
			
            $this->content = $this->url;
        }
		if( empty($this->width) || $this->width == 0 )
		{
			$this->width = 430;
		}
		if( empty($this->height) || $this->height == 0 )
		{
			$this->height = 270;
		}

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if( $this->type == self::TYPE_IFRAME 
			|| $this->type == self::TYPE_VIDEO 
			|| $this->type == self::TYPE_LINK ){
            $this->url = $this->content;
        }

        return parent::afterFind();
    }

	public static function getThemeTypes()
	{		
		return array(
            self::THEMES_LIGHT 			=> Yii::t('JsPanelsModule.base', 'light'),
            self::THEMES_MEDIUM 		=> Yii::t('JsPanelsModule.base', 'medium'),
			self::THEMES_DARK 			=> Yii::t('JsPanelsModule.base', 'dark'),
			self::THEMES_AUTUMNGREEN 	=> Yii::t('JsPanelsModule.base', 'autumngreen'),
            self::THEMES_AUTUMNBROWN 	=> Yii::t('JsPanelsModule.base', 'autumnbrown'),
            self::THEMES_AUTUMNRED 		=> Yii::t('JsPanelsModule.base', 'autumnred'),
			self::THEMES_PRIMARY 		=> Yii::t('JsPanelsModule.base', 'primary'),
			self::THEMES_SUCCESS 		=> Yii::t('JsPanelsModule.base', 'success'),
            self::THEMES_INFO 			=> Yii::t('JsPanelsModule.base', 'info'),
            self::THEMES_WARNING 		=> Yii::t('JsPanelsModule.base', 'warning'),
			self::THEMES_DANGER 		=> Yii::t('JsPanelsModule.base', 'danger'),
        );
	}
	
    public static function getPageTypes()
    {
        return array(
            self::TYPE_HTML 	=> Yii::t('JsPanelsModule.base', 'HTML'),
            self::TYPE_MARKDOWN => Yii::t('JsPanelsModule.base', 'MarkDown'),
            self::TYPE_IFRAME 	=> Yii::t('JsPanelsModule.base', 'IFrame'),
			self::TYPE_VIDEO 	=> Yii::t('JsPanelsModule.base', 'Video'),
			self::TYPE_EMBED 	=> Yii::t('JsPanelsModule.base', 'Embed')
        );
    }

	public static function getPositionTypes()
    {	
        return array(
            self::POSITION_AUTO         => Yii::t('JsPanelsModule.base', 'auto'),
            self::POSITION_CENTER       => Yii::t('JsPanelsModule.base', 'center'),
            self::POSITION_TOPLEFT      => Yii::t('JsPanelsModule.base', 'top left'),
            self::POSITION_TOPCENTER    => Yii::t('JsPanelsModule.base', 'top center'),
			self::POSITION_TOPRIGHT     => Yii::t('JsPanelsModule.base', 'top right'),
            self::POSITION_CENTERRIGHT  => Yii::t('JsPanelsModule.base', 'center right'),
            self::POSITION_BOTTOMRIGHT  => Yii::t('JsPanelsModule.base', 'bottom right'),
            self::POSITION_BOTTOMCENTER => Yii::t('JsPanelsModule.base', 'bottom center'),
			self::POSITION_BOTTOMLEFT   => Yii::t('JsPanelsModule.base', 'bottom left'),
            self::POSITION_CENTERLEFT   => Yii::t('JsPanelsModule.base', 'center left')           
        );
    }

}
