<?php

class m140953_245035_initial extends EDbMigration
{

    public function up()
    {
        $this->createTable('js_panels_page', array(
            'id' => 'pk',
            'type' => 'smallint(6) NOT NULL',
            'position' => 'varchar(100)',
			'width' => 'smallint(6) NOT NULL',
			'height' => 'smallint(6) NOT NULL',
            'title' => 'varchar(255) NOT NULL',
			'theme' => 'varchar(100)',
            'icon' => 'varchar(100)',
            'content' => 'TEXT',
            'sort_order' => 'int(11)',
            'navigation_class' => 'varchar(255) NOT NULL',
                ), '');
    }

    public function down()
    {
        echo "m140930_245035_initial does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
