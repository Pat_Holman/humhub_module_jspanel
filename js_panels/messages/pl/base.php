<?php
return array (
  'Create new Page' => 'Dodaj stronę',
  'Slide Pages' => 'Własne strony',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Odnośnik',
  'MarkDown' => 'Znaczniki MarkDown',
  'Navigation' => 'Nawigacja',
  'No Slide Pages created yet!' => 'Nie stworzono jeszcze własnych stron!',
  'Sort Order' => 'Sortuj',
  'Title' => 'Tytuł',
  'Top Navigation' => 'Górna nawigacja',
  'Type' => 'Rodzaj',
  'User Account Menu (Settings)' => 'Konto użytkownika (Ustawienia)',
);
