<?php	
	$returnArray = array(
		'navigationClass' => $navigationClass,
		'position'        => $position,
		'theme'        	  => $theme,
		'width'           => $width,
		'title'           => $title,
		'height'          => $height			
	);
	
	$returnArray['content'] = '
		<iframe name="iframe" class="iframe" id="iframepage" style="width:98%;height:100%;margin-left:5px;margin-right:5px;" src="'.$url.'"></iframe>
		<style>
			#iframepage {
				position: absolute;
				left: 0;
				top: 5px; 
				border:1px solid #d4d4d4;
				box-sizing:border-box;
				-moz-box-sizing:border-box;
				-webkit-box-sizing:border-box;
				
			}
		</style>
	';

	echo json_encode($returnArray);
	die;
?>
