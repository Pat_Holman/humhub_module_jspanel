<?php
	$returnArray = array(
		'navigationClass' => $navigationClass,
		'position'        => $position,
		'theme'        	  => $theme,
		'width'           => $width,
		'title'           => $title,
		'height'          => $height			
	);
		
	$returnArray['content'] =  '
		<video controls  style="width:100%;">
			<source src="'.$url.'" type="video/mp4">  			
		</video>';
	echo json_encode($returnArray);
	die;
?>